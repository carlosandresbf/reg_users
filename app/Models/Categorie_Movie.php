<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categorie_Movie extends Model
{
    protected $table = 'categories_movies';
    protected $fillable = ['movie_id', 'category_id', 'state_id'];
    protected $guarded = ['id'];
}
