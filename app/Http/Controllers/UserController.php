<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User as User;
use App\Models\State as State;

class UserController extends Controller
{
	public function destroy($id)
	{
		$user = User::find($id);
		$user->delete();
		return redirect()->back();
	}

	public function show (Request $request)
	{
		$users = User::where('name', 'like', '%'.$request->name.'%')->get();
		return \View::make('users/list', compact('users'));
	}

	public function update($id, Request $request)
	{
		$user = User::find($id);
		$user->name = $request->name;
		$user->email = $request->email;
		$user->password = $request->password;
		$user->state_id = $request->state_id;
		$user->save();
		return redirect('user');
	}

	public function edit($id)
	{
		$user = User::find($id);
		$states = State::all();
		return \View::make('users/update', compact('user', 'states'));
	}

    public function index()
    {
    	$users = User::all();
    	return \View::make('users/list', compact('users'));
    }

    public function create()
    {
    	$states = State::all();
    	return \View::make('users/new', compact('states'));
    }

    public function store(Request $request)
    {
    	$user = new User;
    	$user->name = $request->name;
    	$user->email = $request->email;
    	$user->password = $request->password;
    	$user->state_id = $request->state_id;
    	return redirect('user');
    }
}
