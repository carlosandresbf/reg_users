<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie as Movie;
use App\Models\User as User;
use App\Models\State as State;
use Illuminate\Support\Facades\Auth;


class MovieController extends Controller
{
	public function destroy($id)
	{
		$movie = Movie::find($id);
		$movie->delete();
		return redirect()->back();
	}
 
	public function show(Request $request)
	{
		$movies = Movie::where('name', 'like', '%'.$request->name.'%')->get();
		return \View::make('movies/list', compact('movies'));
	}

	public function update($id, Request $request)
	{
		$movie = Movie::find($id);
		$movie->name = $request->name;
		$movie->description = $request->description;
		$movie->user_id = $request->user_id;
		$movie->state_id = $request->state_id;
		$movie->save();
		return redirect('movie');
	}

	public function edit($id)
	{
		$users = User::all();
		$states = State::all();
		$movie = Movie::find($id);
		return \View::make('movies/update', compact('movie', 'users', 'states'));
	}

	public function index()
	{	
		$states = State::all();
		$users = User::all();
		$movies = Movie::all();
		if (Auth::check()) {
    	$show_edit_delete = true;
			}else{
		$show_edit_delete = false;		
			}
		return \View::make('movies/list',compact('movies', 'users', 'states', 'show_edit_delete'));
	}

	public function create()
	{
		$users = User::all();
		$states = State::all();
		return \View::make('movies/new', compact('users', 'states'));
	}

    public function store (Request $request)
    {
    	$movie = new Movie;
    	$movie->name = $request->name;
    	$movie->description = $request->description;
    	$movie->user_id = $request->user_id;
		$movie->state_id = $request->state_id;
    	$movie->save();
    	return redirect('movie');
    }
}
