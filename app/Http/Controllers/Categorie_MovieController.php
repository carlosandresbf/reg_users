<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categorie_Movie as Categorie_Movie; 
use App\Models\Movie as Movie;
use App\Models\State as State;
use App\Models\Categorie as Categorie;

class Categorie_MovieController extends Controller
{
	public function destroy($id)
	{
		$categorie_movie = Categorie_Movie::find($id);
		$categorie_movie->delete();
		return redirect()->back();
	}

	public function show(Request $request)
	{
		$categories_movies = Categorie_Movie::where('movie_id', 'like', '%'.$request->movie_id.'%')->get();
		return \View::make('categories_movies/list', compact('categories_movies'));
	}

	public function update($id, Request $request)
	{
		$categorie_movie = Categorie_Movie::find($id);
		$categorie_movie->movie_id = $request->movie_id;
		$categorie_movie->category_id = $request->category_id;
		$categorie_movie->state_id = $request->state_id;
		return redirect('categorie_movie');
	}

	public function edit($id)
	{
		$categorie_movie = Categorie_Movie::find($id);
		$movies = Movie::all();
		$states = State::all();
		$categories = Categorie::all();
		return \View::make('categories_movies/update', compact('categorie_movie', 'movies', 'states', 'categories'));
	}

	public function index ()
	{
		$categories_movies = Categorie_Movie::all();
		return \View::make('categories_movies/list', compact('categories_movies'));
	}

	public function create()
	{
		$movies = Movie::all();
		$states = State::all();
		$categories = Categorie::all();
		return \View::make('categories_movies/new', compact('movies', 'states', 'categories'));
	}

    public function store(Request $request)
    {
    	$categorie_movie = new Categorie_Movie;
    	$categorie_movie->movie_id = $request->movie_id;
    	$categorie_movie->category_id = $request->category_id;
    	$categorie_movie->state_id = $request->state_id;
    	$categorie_movie->save();
    	return redirect('categorie_movie');
    }
}
