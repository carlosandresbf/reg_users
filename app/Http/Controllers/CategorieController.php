<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categorie as Categorie;
use App\Models\State as State;

class CategorieController extends Controller
{
	public function destroy($id)
	{
		$categorie = Categorie::find($id);
		$categorie->delete();
		return redirect()->back();
	}

	public function show (Request $request)
	{
		$categories = Categorie::where('name', 'like', '%'.$request->name.'%')->get();
		return \View::make('categories/list', compact('categories'));
	}

	public function update($id, Request $request)
	{
		$categorie = Categorie::find($id);
		$categorie->name = $request->name;
		$categorie->state_id = $request->state_id;
		$categorie->save();
		return redirect('categorie');
	}

	public function edit($id)
	{
		$states = State::all();
		$categorie = Categorie::find($id);
		return \View::make('categories/update', compact('categorie', 'states'));
	}

	public function index()
	{
		$categories = Categorie::all();
		return \View::make('categories/list', compact('categories'));
	}

	public function create()
	{
		$states = State::all();
		return \View::make('categories/new', compact('states'));
	}

    public function store(Request $request)
    {
    	$categorie = new Categorie;
    	$categorie->name = $request->name;
    	$categorie->state_id = $request->state_id;
    	$categorie->save();
    	return redirect('categorie');
    }
}
