@extends('layouts.app')
@section('content')
	<section class="container">   
		<div class="row">
			<article class="col-md-12">
				{!! Form::open(['route' => 'movie/show', 'method' => 'post', 'novalidate', 'class' => 'form-inline']) !!}
					<div class="form-group">
						<label>Nombre</label>
						<input type="text" name="name" class="form-control">
					</div>
					<div class="form-control">
						<button type="submit" class="btn btn-default">Buscar</button>
						<a href="{{ route('user.index') }}" class="btn btn-success">Usuarios</a>
						<a href="{{ route('categorie.index') }}" class="btn btn-success">Categorías</a>
						<a href="{{ route('categorie_movie.index') }}" class="btn btn-success">Peliculas con categoria</a>
						<a href="{{ route('movie.index') }}" class="btn btn-primary">Todos</a>
						<a href="{{ route('movie.create') }}" class="btn btn-primary">Crear</a>
					</div>
				{!! Form::close() !!}
			</article>
			<article class="col-md-12">
				<table class="table table-condensed table-striped table-bordered">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Descripción</th>
							<th>Usuario</th>
							<th>State</th>
							<th>Fecha de creación</th>
							<th>Fecha de actualización</th>
							<th>Acción</th>
						</tr>
					</thead>
					<tbody>
						@foreach($movies as $movie)
							<tr>
								<td>{{ $movie->name }}</td>
								<td>{{ $movie->description }}</td>
								<td>{{ $movie->user_id }}</td>
								<td>{{ $movie->state_id }}</td>
								<td>{{ $movie->created_at }}</td>
								<td>{{ $movie->updated_at }}</td>
								<td>
							@if ($show_edit_delete)

									<a href="{{ route('movie.edit', ['id' => $movie->id]) }}" class="btn btn-primary btn-xs">Editar</a>
									<a href="{{ route('movie/destroy',['id' => $movie->id]) }}" class="btn btn-danger btn-xs">Borrar</a>
									 @endif 
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</article>
		</div>
	</section>
@endsection