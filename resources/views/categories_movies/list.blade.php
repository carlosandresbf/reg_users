@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-12">
				{!! Form::open(['route' => 'categorie_movie/show', 'method' => 'post', 'novalidate', 'class' => 'form-inline']) !!}
					<div class="form-group">
						<label>Nombre pelicula</label>
						<input type="text" name="name" class="form-control">
					</div>
					<div class="form-control">
						<button type="submit" class="btn btn-default">Buscar</button>
						<a href="{{ route('user.index') }}" class="btn btn-success">Usuarios</a>
						<a href="{{ route('movie.index') }}" class="btn btn-success">Peliculas</a>
						<a href="{{ route('categorie.index') }}" class="btn btn-success">Categorías</a>
						<a href="{{ route('categorie_movie.index') }}" class="btn btn-primary">Todos</a>
						<a href="{{ route('categorie_movie.create') }}" class="btn btn-primary">Crear</a>
					</div>
				{!! Form::close() !!}
			</article>
			<article class="col-md-12">
				<table class="table table-striped table-condensed table-bordered">
					<thead>
						<tr>
							<th>Pelicula</th>
							<th>Categoría</th>
							<th>Estado</th>
							<th>Fecha de creación</th>
							<th>Fecha de actualización</th>
							<th>Acción</th>
						</tr>
					</thead>
					<tbody>
						@foreach($categories_movies as $categorie_movie)
							<tr>
								<td>{{ $categorie_movie->movie_id }}</td>
								<td>{{ $categorie_movie->category_id }}</td>
								<td>{{ $categorie_movie->state_id }}</td>
								<td>{{ $categorie_movie->created_at }}</td>
								<td>{{ $categorie_movie->updated_at }}</td>
								<td>
									<a href="{{ route('categorie_movie.edit', ['id' => $categorie_movie->id]) }}" class="btn btn-primary">Editar</a>
									<a href="{{ route('categorie_movie/destroy', ['id' => $categorie_movie->id ]) }}" class="btn btn-danger">Borrar</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</article>
		</div>
	</section>
@endsection