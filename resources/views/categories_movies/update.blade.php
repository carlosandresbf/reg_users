@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!! Form::model($categorie_movie, ['route' => ['categorie_movie.update', $categorie_movie->id], 'method' => 'put']) !!}
					<div class="form-group">
						<label>Pelicula</label>	
						<select name="movie_id" class="form-control">
							@foreach($movies as $movie)
								<option value="{{ $movie->id }}">{{ $movie->name }}</option>
							@endforeach
						</select>					
					</div>
					<div class="form-group">
						<label>Categoría</label>
						<select name="category_id" class="form-control">
							@foreach($categories as $categorie)
								<option value="{{ $categorie->id }}">{{ $categorie->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label>Estado</label>
						<select name="state_id" class="form-control">
							@foreach($states as $state)
								<option value="{{ $state->id }}">{{$state->state}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success">Enviar</button>
					</div>
				{!! Form::close() !!}
			</article>
		</div>	   
	</section>
@endsection