@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-12">
				{!! Form::open(['route' => 'categorie/show', 'method' => 'post', 'novalidate', 'class' => 'form-inline']) !!}
					<div class="form-group">
						<label>Nombre</label>
						<input type="text" name="name" class="from-control">
					</div>
					<div class="form-control">
						<button type="submit" class="btn btn-default">Buscar</button>
						<a href="{{ route('movie.index') }}" class="btn btn-success">Peliculas</a>
						<a href="{{ route('categorie_movie.index') }}" class="btn btn-success">Peliculas con categoria</a>
						<a href="{{ route('user.index') }}" class="btn btn-success">Usuarios</a>
						<a href="{{ route('categorie.index') }}" class="btn btn-primary">Todos</a>
						<a href="{{ route('categorie.create') }}" class="btn btn-primary">Crear</a>
					</div>
				{!! Form::close() !!}
			</article>
			<article class="col-md-12">
				<table class="table table-striped table-condensed table-bordered">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Estado</th>
							<th>Fecha de creación</th>
							<th>Fecha de actualización</th>
							<th>Acción</th>
						</tr>
					</thead>
					<tbody>
						@foreach($categories as $categorie)
							<tr>
								<td>{{ $categorie->name }}</td>
								<td>{{ $categorie->state_id }}</td>
								<td>{{ $categorie->created_at }}</td>
								<td>{{ $categorie->updated_at }}</td>
								<td>
									<a href="{{ route('categorie.edit', ['id' => $categorie->id]) }}" class="btn btn-primary">Editar</a>
									<a href="{{ route('categorie/destroy', ['id' => $categorie->id]) }}" class="btn btn-danger">Borrar</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</article>
		</div>
	</section>
@endsection