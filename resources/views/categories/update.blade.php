@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!! Form::model($categorie, ['route' => ['categorie.update', $categorie->id], 'method' => 'put']) !!}				
					<div class="form-group">
						<label>Nombre</label>
						<input type="text" name="name" class="form-control" value=" {{$categorie->name}}">
					</div>
					<div class="form-group">
						<label>Estado</label>
						<select name="state_id" class="form-control">
							@foreach($states as $state)
								<option value="{{$state->id}}">{{ $state->state }} </option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success">Enviar</button>
					</div>
				{!! Form::close() !!}
			</article>
		</div>
	</section>
@endsection