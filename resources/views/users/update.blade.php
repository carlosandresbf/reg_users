@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'put']) !!}
					<div class="form-group">
						<label>Nombre</label>
						<input type="text" name="name" class="form-control" value="{{$user->name}}" required>
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" name="email" class="form-control" required value="{{ $user->email}}">
					</div>
					<div class="form-group">
						<label>Contraseña</label>
						<input type="password" name="password" class="form-control" required value="{{$user->password}}">
					</div>
					<div class="form-group">
						<label>Estado</label>
						<select name="state_id" class="form-control">
							@foreach($states as $state)
								<option value="{{ $state->id }}">{{ $state->state }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success">Enviar</button>
					</div>
				{!! Form::close() !!}
			</article>
		</div>
	</section>
@endsection