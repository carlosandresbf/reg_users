@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-12">
				{!! Form::open(['route' => 'user/show', 'method' => 'post', 'novalidate', 'class' => 'form-inline']) !!}
					<div class="form-group">
						<label>Nombre</label>
						<input type="text" name="name" class="form-control">
					</div>
					<div class="form-control">
						<button type="submit" class="btn btn-default">Buscar</button>
						<a href="{{ route('movie.index') }}" class="btn btn-success">Peliculas</a>
						<a href="{{ route('categorie.index') }}" class="btn btn-success">Categorías</a>
						<a href="{{ route('categorie_movie.index') }}" class="btn btn-success">Peliculas con categoria</a>
						<a href="{{ route('user.index') }}" class="btn btn-primary">Todos</a>
						<a href="{{ route('user.create') }}" class="btn btn-primary">Crear</a>
					</div>
				{!! Form::close() !!}
			</article>
			<article class="col-md-12">
				<table class="table table-striped table-condensed table-bordered">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Email</th>
							<th>Contraseña</th>
							<th>Estado</th>
							<th>Fecha de creación</th>
							<th>Fecha de actualización</th>
							<th>Acción</th>
						</tr>
					</thead>
					<tbody>
						@foreach($users as $user)
							<tr>
								<td>{{ $user->name }}</td>
								<td>{{ $user->email }}</td>
								<td>{{ $user->password }}</td>
								<td>{{ $user->state_id }}</td>
								<td>{{ $user->created_at }}</td>
								<td>{{ $user->updated_at }}</td>
								<td>
									<a href="{{ route('user.edit', ['id' => $user->id]) }}" class="btn btn-primary btn-xs">Editar</a>
									<a href="{{ route('user/destroy', ['id' => $user->id]) }}" class="btn btn-danger btn-xs">Borrar</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</article>
		</div>
	</section>
@endsection