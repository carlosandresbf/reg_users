<?php
Auth::routes();

Route::resource('movie', 'MovieController');

Route::get('movie/destroy/{id}', ['as' => 'movie/destroy', 'uses' => 'MovieController@destroy']);

Route::post('movie/show', ['as' => 'movie/show', 'uses' => 'MovieController@show']);
Route::resource('user', 'UserController');

Route::get('user/destroy/{id}', ['as' => 'user/destroy', 'uses' => 'UserController@destroy']);

Route::post('user/show', ['as' => 'user/show', 'uses' => 'UserController@show']);

//Categories
Route::resource('categorie', 'CategorieController');
Route::get('categorie/destroy/{id}', ['as' => 'categorie/destroy', 'uses' => 'CategorieController@destroy']);
Route::post('categorie/show', ['as' => 'categorie/show', 'uses' => 'CategorieController@show']);
Route::post('categorie/show', ['as' => 'categorie/store', 'uses' => 'CategorieController@store']);
Route::post('categorie/show', ['as' => 'categorie/update', 'uses' => 'CategorieController@update']);


