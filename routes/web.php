<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Otros
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');




//Route::resource('states', 'StatesController');



//Route::post('state/show', ['as' => 'state/show', 'uses' => 'StatesController@show']);
//Categorie Movie
/*Route::resource('categorie_movie', 'Categorie_MovieController');

Route::post('categorie_movie/show', ['as' => 'categorie_movie/show', 'uses' => 'Categorie_MovieController@show']);

Route::get('categorie_movie/destroy/{id}', ['as' => 'categorie_movie/destroy', 'uses' => 'Categorie_MovieController@destroy']);*/
