<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
    	for($i = 0; $i < 10; $i++){
    		$user = new User; 
    		$user->name = $faker->name;
    		$user->email = $faker->email;
    		$user->password = Hash::make('test123');
            $user->role = $faker->randomElement($array = array('Admin','User'));
    		$user->state_id = 1;
    		$user->save();
    	}
    }
}
