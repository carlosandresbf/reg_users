<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Categorie;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i = 0; $i < 10; $i++){
        	$categorie = new Categorie;
        	$categorie->name = 'Acción';
        	$categorie->state_id = 1;
        	$categorie->save();
        }
    }
}
