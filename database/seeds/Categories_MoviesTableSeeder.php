<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Categorie_Movie;

class Categories_MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i = 0; $i < 10; $i++){
        	$categorie_movies = new Categorie_Movie;
        	$categorie_movies->movie_id = 8;
        	$categorie_movies->category_id = 5;
        	$categorie_movies->state_id = 2;
        	$categorie_movies->save();
        }
    }
}
